
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit'))
channel = connection.channel()

channel.queue_declare(queue='test')

m = input("Message: ")

channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=m)
print(" [x] Sent " + m)

connection.close()