# Docker Compose Beispiel: RabbitMQ / Python 

## Start 
```
docker-compose up 
```

## Service 1 (Senden)
Script fürs Senden ausführen: 
```
docker-compose exec service1 bash -c "python send.py"
```

## Service 2 (Empfangen)
Script zum Empfangen ausführen:
```
docker-compose exec service2 bash -c "python receive.py"
```
Beenden des Scripts mit `STRG + C`

## Beenden 
```
docker-compose down
``` 

